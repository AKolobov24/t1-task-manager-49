package ru.t1.akolobov.tm.api.endpoint;

public interface IConnectionProvider {

    String getServerHost();

    String getServerPort();

}
