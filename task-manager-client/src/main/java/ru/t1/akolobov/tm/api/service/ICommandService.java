package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.ICommandRepository;
import ru.t1.akolobov.tm.command.AbstractCommand;

public interface ICommandService extends ICommandRepository {

    void add(@Nullable final AbstractCommand command);

    @Nullable
    AbstractCommand getCommandByName(@Nullable final String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable final String argument);

}
