package ru.t1.akolobov.tm.logger.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OperationEvent {

    private String table;

}
