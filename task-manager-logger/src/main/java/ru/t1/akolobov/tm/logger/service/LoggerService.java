package ru.t1.akolobov.tm.logger.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.LinkedHashMap;
import java.util.Map;

public class LoggerService {

    private final ObjectMapper objectMapper = new YAMLMapper();

    @SneakyThrows
    public void log(String message) {
        final Map<String, Object> event = objectMapper.readValue(message, LinkedHashMap.class);
        final String table = event.get("table").toString();
        final byte[] bytes = message.getBytes();
        final File file = new File(table);
        file.createNewFile();
        Files.write(Paths.get(file.getPath()), bytes, StandardOpenOption.APPEND);
        System.out.println(message);
    }

}
