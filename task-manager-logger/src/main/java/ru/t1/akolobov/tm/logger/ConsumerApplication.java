package ru.t1.akolobov.tm.logger;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import ru.t1.akolobov.tm.logger.listener.EntityListener;
import ru.t1.akolobov.tm.logger.service.LoggerService;

import javax.jms.*;

public class ConsumerApplication {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;
    private static final String QUEUE = "LOGGER";

    public static void main(String[] args) throws JMSException {
        final LoggerService loggerService = new LoggerService();
        final EntityListener entityListener = new EntityListener(loggerService);

        final ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory(URL);
        final Connection connection = connectionFactory.createConnection();
        final Session session;
        final Queue destination;

        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        final MessageConsumer consumer = session.createConsumer(destination);
        consumer.setMessageListener(entityListener);
    }

}
