package ru.t1.akolobov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.service.IExternalLogService;
import ru.t1.akolobov.tm.dto.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExternalLogService implements IExternalLogService {

    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    private static final String QUEUE = "LOGGER";

    private final BrokerService brokerService = new BrokerService();

    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    private final Connection connection;

    private final Session session;

    private final Queue destination;

    private final MessageProducer messageProducer;

    private final ExecutorService executorService = Executors.newCachedThreadPool();

    private final ObjectMapper objectMapper = new YAMLMapper();

    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @SneakyThrows
    public ExternalLogService() {
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.start();
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
    }

    @Override
    public void send(@NotNull final OperationEvent event) {
        executorService.submit(() -> sync(event));
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            final Table table = entityClass.getAnnotation(Table.class);
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

    @Override
    @SneakyThrows
    public void stop() {
        executorService.shutdown();
        session.close();
        connection.close();
        brokerService.stop();
    }

}
