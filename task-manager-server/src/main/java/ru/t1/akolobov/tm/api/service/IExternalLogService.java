package ru.t1.akolobov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.dto.OperationEvent;

public interface IExternalLogService {

    void send(@NotNull final OperationEvent event);

    void stop();

}
